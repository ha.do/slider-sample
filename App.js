import React from 'react';
import { Dimensions, StyleSheet, View } from "react-native";
import Animated from "react-native-reanimated";
import LinearGradient from 'react-native-linear-gradient';

import Cursor from "./src/components/Cursor";
import Labels from "./src/components/Labels";

const { Value, max, add } = Animated;

const { width: totalWidth } = Dimensions.get("window");
const count = 30;
const width = totalWidth / count;
const height = width+50;
const styles = StyleSheet.create({
  container: {
    width: totalWidth,
    height,
    backgroundColor: "#f1f2f6",
    marginTop: 100,
  },
});


const App: () => React$Node = () => {
  const [i, setI] = React.useState(0);
  let x = new Value(0);
  const handleTap = (i) => {
    setI(i);
    x = new Value(0);
  };
  
  return (
    <View style={styles.container}>
      <Animated.View
        style={{
          position: "absolute",
          top: 0,
          left: 0,
          right: 0,
          width: add(max(i*width, 0), width),
          height,
        }}
      >
        <LinearGradient colors={['#4a9bbb', '#9c71bf']} start={{x: 0, y: 0}} end={{x: 1, y: 0}} style={{ backgroundColor: 'black', width: '100%', height: '100%' }}></LinearGradient>
        </Animated.View>
      <Labels size={height} {...{ x, count }} tabEvent={handleTap}/>
      <Cursor size={width} {...{ x, count, current: i }} panEvent={setI}/>
    </View>
  );
};

export default App;
