import * as React from "react";
import { StyleSheet, View, TouchableWithoutFeedback } from "react-native";
import { mixColor } from "react-native-redash";
import Animated from "react-native-reanimated";

const { cond, lessOrEq, add, round, divide } = Animated;

interface LabelProps {
  x: Animated.Value<number>;
  count: number;
  size: number;
}

export default ({ count, x, size, tabEvent, panEvent }: LabelProps) => {
  const index = add(round(divide(x, size)), 1);
  return (
    <View
      style={{
        ...StyleSheet.absoluteFillObject,
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      {new Array(count).fill(0).map((e, i) => {
        const color = mixColor(cond(lessOrEq(index, i), 0, 1), "gray", "gray");
        return (
          <View key={i} style={{ flex: 1 }}>
            <TouchableWithoutFeedback onPress={() => tabEvent(i)}>
            <Animated.View style={{ color, textAlign: "center", fontSize: 24 }}>
              <View style={{ backgroundColor: 'white', width: 8, height: '100%' }}></View>
            </Animated.View>
            </TouchableWithoutFeedback>
          </View>
        );
      })}
    </View>
  );
};